name := "messagebox-api"

version := "1.0"

//scalaVersion := "2.12.1"
scalaVersion := "2.11.8"

// This is necessary for Lucidchart's relate project
resolvers += "Sonatype release repository" at "https://oss.sonatype.org/content/repositories/releases/"

libraryDependencies ++= Seq(
  "org.json4s" %% "json4s-native" % "3.5.0",
  "com.typesafe.akka" %% "akka-actor" % "2.4.16",
  "com.typesafe.akka" %% "akka-http-core" % "10.0.1",
  "com.typesafe.akka" %% "akka-http" % "10.0.1",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.1",
  "com.typesafe.akka" %% "akka-http-jackson" % "10.0.1",
  "com.typesafe.akka" %% "akka-http-xml" % "10.0.1",
  "com.lucidchart" %% "relate" % "1.13.0",
  // Something is messed up with the Maven repo for log4j-api-scala, so here is a fallback:
  "org.apache.logging.log4j" % "log4j-core" % "2.7",
  "org.apache.logging.log4j" % "log4j-api" % "2.7",
  "org.apache.logging.log4j" %% "log4j-api-scala" % "2.7",
  "org.postgresql" % "postgresql" % "9.4.1212",
  "com.zaxxer" % "HikariCP" % "2.5.1",
  "de.mkammerer" % "argon2-jvm" % "2.1"
)

flywayUser := "postgres"
flywayPassword := ""
flywayUrl := "jdbc:postgresql://localhost:5432/messagebox"

