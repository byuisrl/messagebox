# MessageBox

MessageBox implements a simple message storage API.

## Setup (Fedora 24)

1. Install SBT
2. `sbt run`
3. Yep, it should be that simple.

### Fedora 24
```
curl https://bintray.com/sbt/rpm/rpm > bintray-sbt-rpm.repo
sudo mv bintray-sbt-rpm.repo /etc/yum.repos.d/
sudo dnf install sbt
```

### Other

Get SBT from [http://www.scala-sbt.org/download.html](http://www.scala-sbt.org/download.html)
