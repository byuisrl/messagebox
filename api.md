# MessageBox API

All POST requests must be made with the header `Content-type: application/json`.  

---
## Users

`POST /users`  Creates a new user, if possible.

### Parameters
```
{
    "username": <string>
    "password": <string>
}
```
### Returns
`409` if the username is already taken.

`201` if the user was created. The client may then login separately through `/login`.

---
`GET /users` If enabled, returns a list of users.  Implementation of this method is optional.
### Returns
`405` if this method has been disabled in configuration
`200` and response body:

```
{
    "users": [
        <user>, <user>, <user>
    ]
}
```

---

## Login

`POST /login`

### Parameters
```
{
    "username": <string>,
    "password": <string>
}
```
### Returns
`401` if the credentials are invalid.

`200` if the credentials are valid, along with a JSON body:
```
{
    "token": <string>
}
```

Subsequent API calls must be authenticated by passing an `Authorization` header:

```
Authorization:  Bearer <string>
```

---

# TODO: Finish details on these

`GET /messages`  Returns a list of `<message_short>` objects for the currently logged in user.
`GET /messages/<id>`  Returns a `<message>` object if the user is the sender or receiver.
`GET /messages?type=sent` filters the list of `<message_short>` objects for the user
`POST /messages`  Sends the message.  TODO: Support drafting of messages.