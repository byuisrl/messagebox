CREATE TABLE users (
    id BIGSERIAL,
    username VARCHAR(64) UNIQUE NOT NULL,
    pwhash VARCHAR(96) NOT NULL,
    PRIMARY KEY(id)
);

