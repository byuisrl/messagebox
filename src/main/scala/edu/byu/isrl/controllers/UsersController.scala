package edu.byu.isrl.controllers

import akka.actor.ActorSystem
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{HttpEntity, HttpResponse}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import spray.json.DefaultJsonProtocol

import scala.concurrent.Future

/**
  * Created by krr428 on 1/12/17.
  */

class LoginController(system: ActorSystem) extends SprayJsonSupport with DefaultJsonProtocol {

  final case class LoginParameters(username: String, password: String)

  private implicit val loginParameterFormat = jsonFormat2(LoginParameters)
//  private implicit val executionContext = //TODO: Get a better threading system worked out.

  def login(loginParameters: LoginParameters): Future[HttpResponse] = {
    null
  }

  val routes: Route =
    path("login") {
      post {
        decodeRequest {
          entity(as[LoginParameters]) { loginParams => complete(this.login(loginParams)) }
        }
      }
    }

}

object LoginController {
  def apply(system: ActorSystem): LoginController = new LoginController(system)
}


class UsersController {

}


