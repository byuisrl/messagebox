package edu.byu.isrl

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import org.apache.logging.log4j.scala._
import edu.byu.isrl.controllers.LoginController
import edu.byu.isrl.models.UserModel

import scala.concurrent.Future

object Main {

  val logger = Logger(this.getClass)


  def main(args: Array[String]): Unit = {

    println(UserModel.createUser("krrg", "password1".toCharArray))
    println(UserModel.createUser("krrg", "password2".toCharArray))

//    logger.info("Starting server...")
//
//    implicit val system = ActorSystem("messagebox-system")
//    implicit val materializer = ActorMaterializer()
//    implicit val executionContext = system.dispatcher
//
//    val route =
//      path("hello") {
//        get {
//          complete(
//            Future {
//              HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Hello world</h1>")
//            }
//          )
//        }
//      } ~ LoginController(system).routes
//
//    Http().bindAndHandle(route, "localhost", 8080)
  }

}
