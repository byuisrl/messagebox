package edu.byu.isrl.models

import java.security.SecureRandom
import java.sql.SQLException

import com.lucidchart.open.relate.SqlResult
import com.lucidchart.open.relate.interp._
import de.mkammerer.argon2.Argon2Factory


trait PasswordHelper {

  private val random = new SecureRandom()
  private val argon_iterations = 3
  private val argon_memory = 65536
  private val argon_parallelism = 1
  private val argon2 = Argon2Factory.create()

  def passwordHash(password: Array[Char]): String = {
    val pwhash = argon2.hash(3, 65536, 1, password) /* Salt is generated automatically from SecureRandom*/
    argon2.wipeArray(password)
    pwhash
  }

  def passwordVerify(password: Array[Char], passwordHash: String): Boolean = {
    argon2.verify(passwordHash, password)
  }

}

case class User(id: Long, username: String)


object UserModel extends Model with PasswordHelper {

  private def asUser(row: SqlResult): User = {
    User(
      row.long("id"),
      row.string("username")
    )
  }

  def findById(id: Long): Option[User] = {
    sql"SELECT id, username FROM users WHERE id = $id".asSingleOption(asUser)(getConnection)
  }

  def findByUsername(username: String): Option[User] = {
    /* we always store usernames in lowercase */
    sql"SELECT id, username FROM users WHERE username = ${username.toLowerCase}".asSingleOption(asUser)(getConnection)
  }

  /**
    * Creates a new user if possible.  The username must be unique.
    * @param username
    * @param password
    * @return
    */
  def createUser(username: String, password: Array[Char]): Boolean = {
    val pwhash = passwordHash(password)
    try {
      sql"INSERT INTO users(username, pwhash) VALUES (${username.toLowerCase}, $pwhash)".execute()(getConnection)
    } catch {
      case ex: SQLException => {

        if (ex.getSQLState == "23505" /* UNIQUE constraint violation */) {
          true
        } else {
          println("Else was met")
          println(ex.getSQLState)
          throw ex /* Then something more catastrophic has happened */
        }

      }
    }
  }

  def verifyPassword(username: String, password: Array[Char]): Boolean = {
    val optPwHash = sql"SELECT pwhash FROM users WHERE username = ${username.toLowerCase}"
      .asSingleOption(row => row.string("pwhash"))(getConnection)

    optPwHash match {
      case Some(pwhash) => passwordVerify(password, pwhash)
      case None => false
    }
  }


}
