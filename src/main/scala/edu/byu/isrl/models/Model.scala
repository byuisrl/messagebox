package edu.byu.isrl.models

import java.sql.Connection

import com.typesafe.config.{Config, ConfigFactory}
import com.zaxxer.hikari.HikariDataSource

/**
  * Created by krr428 on 1/12/17.
  */

object ConnectionHolder {

  private val config: Config = ConfigFactory.defaultApplication()

  private val pgUser = config.getString("db.postgres.username")
  private val pgPass = config.getString("db.postgres.password")
  private val pgHost = config.getString("db.postgres.host")
  private val pgPort = config.getInt("db.postgres.port")
  private val pgDatabase = config.getString("db.postgres.database")

  private val connectionString = s"jdbc:postgresql://${pgHost}:${pgPort}/${pgDatabase}"

  val hikariDataSource: HikariDataSource = new HikariDataSource()
  hikariDataSource.setJdbcUrl(connectionString)
  hikariDataSource.setUsername(pgUser)
  hikariDataSource.setPassword(pgPass)

}

trait Model {

  /* TODO: This should be improved to be more resiliant to blocking and errors. */
  def getConnection: Connection = {
    ConnectionHolder.hikariDataSource.getConnection
  }

}
